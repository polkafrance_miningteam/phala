Pour ajouter les modifications du Repository dans la section Wiki de Gitlab,
un membre (rôle développeur minimum) du projet doit réaliser les commandes suivantes :

`git clone https://gitlab.com/polkafrance_miningteam/phala.git`

`cd phala`

`git push -f https://gitlab.com/polkafrance_miningteam/phala.wiki.git`
